// cookies
$(function () {
  var cookieName = 'cookiesok';
  var cookiesStr = '; '+ document.cookie +';';
  var index = cookiesStr.indexOf('; ' + cookieName + '=');
  if (index === -1) {
    $('#cookie-info').show();
  }

  $('#cookie-info-close').click(function() {
    var expires = new Date();
    expires.setTime((new Date()).getTime() + 3600000*24*365);
    document.cookie = cookieName + '=1;expires=' + expires;
    $("#cookie-info").slideUp();
  });
});

// helpers
$.prototype.disable = function () {
  this.prop("disabled", true);
  return this;
};

$.prototype.enable = function () {
  this.prop("disabled", false);
  return this;
};

// chat
$(function() {
  var socket = io.connect(config.socketUrl);
  var talkingNow = false;

  function __(str) {
    if (locale[str]) {
      return locale[str];
    }
    return str;
  }

  function escapeRegExp(str) {
    return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
  }

  var AudioMgr = {
    audioConn: $('#conn-audio').get(0),
    audioMsg: $('#msg-audio').get(0),
    soundsEnabled: true,

    updateSoundVolumeIcon : function () {
      if (!this.soundsEnabled) {
        $("#sound-volume-icon")
          .removeClass('glyphicon-volume-up')
          .removeClass('glyphicon-volume-down')
          .addClass('glyphicon-volume-off');
      } else if (this.audioConn.volume < 0.5) {
        $("#sound-volume-icon")
          .removeClass('glyphicon-volume-off')
          .removeClass('glyphicon-volume-up')
          .addClass('glyphicon-volume-down');
      } else {
        $("#sound-volume-icon")
          .removeClass('glyphicon-volume-off')
          .removeClass('glyphicon-volume-down')
          .addClass('glyphicon-volume-up');
      }
    },

    setVolume : function (volume) {
      this.audioConn.volume = this.audioMsg.volume = volume;
      this.soundsEnabled = (volume > 0);
      this.updateSoundVolumeIcon();
      localStorage.volume = volume;
    },

    changeVolumeAndPlayTestSound : function (volume) {
      AudioMgr.setVolume(volume);
      AudioMgr.playMsgSound();
    },

    toggleSound : function () {
      this.soundsEnabled = !this.soundsEnabled;
      this.updateSoundVolumeIcon();
    },

    playMsgSound : function () {
      if (this.soundsEnabled) {
        this.audioMsg.play();
      }
    },

    playConnSound : function () {
      if (this.soundsEnabled) {
        this.audioConn.play();
      }
    },

    loadSounds : function () {
      AudioMgr.audioConn.load();
      AudioMgr.audioMsg.load();

      $(window)
        .off('keydown', AudioMgr.loadSounds)
        .off('mousedown', AudioMgr.loadSounds)
        .off('touchstart', AudioMgr.loadSounds);
    }
  };

  if (localStorage.volume) {
    $("#sound-volume").val(localStorage.volume * 100);
    AudioMgr.setVolume(localStorage.volume);
  }

  // Fix for Chrome for Andriod issue 178297
  // Based on https://github.com/MauriceButler/simple-audio/
  $(window)
    .on('keydown', AudioMgr.loadSounds)
    .on('mousedown', AudioMgr.loadSounds)
    .on('touchstart', AudioMgr.loadSounds);

  var Stats = {
    setWaitingCount : function (count) {
      $("#stats-waiting-count").text(count);
      if (count > 0) {
        $("#stats-waiting").removeClass("none").addClass("some");
      } else {
        $("#stats-waiting").removeClass("some").addClass("none");
      }
    },

    setTalkingCount : function (count) {
      $("#stats-talking-count").text(count);
    },

    setConnectedCount : function (count) {
      $("#stats-connected-count").text(count);
    },

    setUniqueCount : function (count) {
      $("#stats-unique-count").text(count);
    },

    reset : function () {
      Stats.setWaitingCount(0);
      Stats.setTalkingCount(0);
      Stats.setConnectedCount(0);
      Stats.setUniqueCount(0);
    }
  };

  var Chat = {
    wasScrolledToBottom: true,
    emotIcons: {},

    scrollToBottom : function () {
      if (this.wasScrolledToBottom) {
        var $container = $("#chat-body");
        var scrollHeight = $container.prop('scrollHeight');
        $container.scrollTop(scrollHeight);
      }
    },

    clearAndFocusInput : function () {
      $("#message-field").val("").focus();
    },

    _parseLinks : function (textHtml) {
      return textHtml.replace(/(https?:\/\/[^\s]+)/g, function (url) {
        url = $('<div></div>').html(url).text(); // unescape
        return $('<a target="_blank"></a>')
          .attr('href', url)
          .text(url)
          .prop('outerHTML');
      });
    },

    _parseEmoticons : function (textHtml) {
      for (name in this.emotIcons) {
        var emoticon = this.emotIcons[name];
        textHtml = textHtml.replace(emoticon.regexp, function () {
          return $('<img class="emoticon">')
            .attr('src', emoticon.url)
            .attr('alt', name)
            .attr('title', name)
            .prop('outerHTML');
        });
      }
      return textHtml;
    },

    showMessage : function (type, nick, text) {
      var now = new Date();
      var hh = now.getHours();
      var mm = now.getMinutes();
      var ss = now.getSeconds();
      if (hh < 10) {
        hh = '0' + hh;
      }
      if (mm < 10) {
        mm = '0' + mm;
      }
      if (ss < 10) {
        ss = '0' + ss;
      }
      var timestamp = "["+hh+":"+mm+":"+ss+"] ";

      // parse links and emoticons in message
      var $textElement = $('<span class="msg-text"></span>').text(text);
      var textHtml = $textElement.html();
      textHtml = this._parseLinks(textHtml);
      textHtml = this._parseEmoticons(textHtml);
      if ($.trim(textHtml) === '')
        textHtml = '&nbsp;';
      $textElement.html(textHtml);

      var $msg = $('<div class="msg"></div>').addClass(type)
        .append($('<span class="msg-time"></span>').text(timestamp))
        .append($('<span class="msg-nick"></span>').text(nick));
      if (nick)
        $msg.append($('<span class="msg-colon">: </span>'));
      $msg.append($textElement);

      $("#msg-container").append($msg);
      Chat.scrollToBottom();
    },

    msgStranger : function (text) {
      AudioMgr.playMsgSound();
      Chat.showMessage("stranger", __("Nieznajomy"), text);
    },

    msgYou : function (text) {
      Chat.showMessage("you", __("Ty"), text);
    },

    msgInfo : function (text) {
      Chat.showMessage("info", "", text);
    },

    showWriting : function () {
      $('#msg-writing').stop().fadeIn(400);
      Chat.scrollToBottom();
    },

    hideWriting : function () {
      $("#msg-writing").stop().fadeOut(400);
    },

    removeWriting : function () {
      $("#msg-writing").stop().hide();
    },

    insertInput : function (text) {
      var $input = $('#message-field');
      var caretPos = $input.prop('selectionStart');
      var fullText = $input.val();
      $input.val(fullText.substring(0, caretPos) + text + fullText.substring(caretPos));
      $input.prop('selectionStart', caretPos + text.length).prop('selectionEnd', caretPos + text.length);
      $input.focus();
    }
  };

  $("#chat-body").on('scroll', function () {
    // Note: we need to add 1 because innerHeight can be inaccurate and fractional
    var $this = $(this);
    Chat.wasScrolledToBottom = ($this.scrollTop() + $this.innerHeight() + 1 >= this.scrollHeight);
  });

  // build emoticons list
  $('.emoticon').each(function() {
    Chat.emotIcons[this.title] = {
      url: this.src,
      regexp: new RegExp(escapeRegExp(this.title), 'g')
    };
  });

  function modeDisconnected () {
    $("#connect-btn").addClass("btn-primary").removeClass("btn-default").text(__("Połącz")).disable();
    $("#message-field").disable().val("").attr('placeholder', __("Łączenie z serwerem..."));
    $("#send-btn").addClass("btn-default").removeClass("btn-primary").disable();
    $("#emoticons-btn").disable();
    $("#emoticons-panel").slideUp();
    talkingNow = false;
  }

  function modeChatLobby () {
    $("#connect-btn").addClass("btn-primary").removeClass("btn-default").text(__("Połącz")).enable()[0].onclick = getStranger;
    $("#message-field").disable().val("").attr('placeholder', __("Aby się połączyć, kliknij niebieski przycisk :)"));
    $("#send-btn").addClass("btn-default").removeClass("btn-primary").disable();
    $("#emoticons-btn").disable();
    $("#emoticons-panel").slideUp();
    talkingNow = false;
  }

  function modeGettingStranger () {
    $("#connect-btn").addClass("btn-primary").removeClass("btn-default").text(__("Rozłącz")).enable()[0].onclick = disconnect;
    $("#message-field").disable().val("").attr('placeholder', __("Łączenie... Aby przerwać szukanie nieznajomego, kliknij \"Rozłącz\"."));
    $("#send-btn").removeClass("btn-primary").addClass("btn-default").disable();
    $("#emoticons-btn").disable();
    $("#emoticons-panel").slideUp();
  }

  function modeChatting () {
    $("#connect-btn").addClass("btn-default").removeClass("btn-primary").text(__("Rozłącz")).enable()[0].onclick = modeEndChatConfirm;
    $("#message-field").enable().attr('placeholder', __("Wpisz wiadomość..."));
    $("#send-btn").addClass("btn-primary").removeClass("btn-default").enable()[0].onclick = sendChatMsg;
    $("#emoticons-btn").enable();
    talkingNow = true;
    Chat.clearAndFocusInput();
  }

  function modeEndChatConfirm () {
    $("#connect-btn").removeClass("btn-default").addClass("btn-primary").text(__("Na pewno?")).enable()[0].onclick = disconnect;
    $("#send-btn").removeClass("btn-primary").addClass("btn-default").enable();
  }

  function getStranger () {
    modeGettingStranger();
    Chat.msgInfo(__("Szukanie nieznajomego... proszę, poczekaj :)"));
    socket.emit("get_stranger");
  }

  function disconnect () {
    modeChatLobby();
    socket.emit("end_chat");
    Chat.removeWriting();
    Chat.msgInfo(__("Rozłączono."));
  }

  function sendChatMsg () {
    writing = false;
    socket.emit("writing", false);
    var msg = $("#message-field").val();
    if (msg) {
      socket.emit("msg", msg);
      Chat.msgYou(msg);
    }
    Chat.clearAndFocusInput();
  }


  // Notifications

  var windowIsActive = true;
  var titleTimer = false;
  var origTitle = document.title;

  function stopNotification() {
    if (titleTimer) {
      clearInterval(titleTimer);
      document.title = origTitle;
    }
  }

  $(window).focus(function() {
    windowIsActive = true;
    stopNotification();
  });
  $(window).blur(function() {
    windowIsActive = false;
  });

  function showNotification(text) {
    if (windowIsActive) {
      return;
    }

    if ('Notification' in window && Notification.permission == "granted") {
      // use Notification API
      var title = '6pony.pl';
      var options = {
        body: text,
        tag: '6pony',
        icon: 'img/favicon.png'
      };

      // Note: "new Notification" throws exception in Chrome Mobile (issue 481856)
      try {
        var notification = new Notification(title, options);
        setTimeout(function() {
          notification.close();
        }, 5000);
      } catch (error) {
        // its Chrome Mobile - classic notifications doesn't work too
        return;
      }
    } else {
      // classic notification
      stopNotification();
      var oldTitle = origTitle;
      document.title = text + ' - 6pony.pl';
      titleTimer = setInterval(function () {
        var tempTitle = document.title;
        document.title = oldTitle;
        oldTitle = tempTitle;
      }, 1000);
    }
  }

  if ('Notification' in window) {
    Notification.requestPermission();
  }

  socket.on("waiting_count", function (count) {
    Stats.setWaitingCount(count);
  });

  socket.on("talking_count", function (count) {
    Stats.setTalkingCount(count);
  });

  socket.on("connected_count", function (count) {
    Stats.setConnectedCount(count);
  });

  socket.on("unique_count", function (count) {
    Stats.setUniqueCount(count);
  });

  function formatDate(date) {
    var monthNames = [
      __("Styczeń"), __("Luty"), __("Marzec"),
      __("Kwiecień"), __("Maj"), __("Czerwiec"), __("Lipiec"),
      __("Sierpień"), __("Wrzesień"), __("Październik"),
      __("Listopad"), __("Grudzień")
    ];

    var day = date.getDate();
    var monthIndex = date.getMonth();
    var year = date.getFullYear();

    return day + ' ' + monthNames[monthIndex] + ' ' + year;
  }

  function zeroPad(num, size) {
    var s = "000000000" + num;
    return s.substr(s.length-size);
  }

  function formatTime(date) {
    var hours = date.getHours();
    var min = zeroPad(date.getMinutes(), 2);
    return hours + ":" + min;
  }

  socket.on("last_visit_time", function (time, usersCount) {
    if (usersCount > 1) {
      Chat.msgInfo(__("Wygląda na to, że nie jesteś tu sam(a)! Życzymy dobrej zabawy :)"));
    } else {
      Chat.msgInfo(__("Wygląda na to, że jesteś tu sam(a) :( Poczekaj chwilę na nowego użytkownika. Pamiętaj, że gdy " +
        "każdy rozłącza się od razu po zobaczeniu, że czat jest pusty, to jest bardzo mała szansa na sparowanie... " +
        "Możesz też nas polecić znajomym, za co będziemy bardzo wdzięczni."));
      if (time) {
        var cur_time = new Date().getTime();
        var diff = Math.floor((cur_time - time) / 1000);
        if (diff < 60) {
          Chat.msgInfo(__("Ostatnio ktoś nas odwiedził ") + diff + __(" sekund temu, a więc przed chwilą się mineliście!"));
        } else if (diff < 3600) {
          Chat.msgInfo(__("Ostatnio ktoś nas odwiedził ") + Math.floor(diff/60) + __(" minut temu!"));
        } else if (diff < 3600) {
          Chat.msgInfo(__("Ostatnio ktoś nas odwiedził ") + Math.floor(diff/3600) + __(" godzin temu!"));
        } else {
          var date = new Date(time);
          Chat.msgInfo(__("Ostatnio ktoś nas odwiedził dnia ") + formatDate(date) + " o godzinie " + formatTime(date));
        }
      }
    }
  });

  socket.on("found_stranger", function () {
    AudioMgr.playConnSound();
    modeChatting();
    Chat.msgInfo(__("Połączono! Przywitaj się, miłej zabawy :3"));
    showNotification(__("Połączono z nieznajomym"));
  });

  socket.on("stranger_disconnected", function () {
    modeChatLobby();
    Chat.removeWriting();
    Chat.msgInfo(__("Nieznajomy się rozłączył... :("));
  });

  socket.on("msg", function (text) {
    Chat.removeWriting();
    Chat.msgStranger(text);
    showNotification(__("Otrzymano wiadomość: ") + "\n" + text);
  });

  socket.on("writing", function (writing) {
    if (writing) {
      Chat.showWriting();
    } else {
      Chat.hideWriting();
    }
  });

  socket.on("announcement", function (text) {
    Chat.msgInfo(text);
  });

  var wasConnected = false;
  socket.on("connect", function () {
    modeChatLobby();
    if (wasConnected) {
      Chat.msgInfo(__("Połączono z serwerem."));
    }
    wasConnected = true;
  });

  socket.on("disconnect", function () {
    modeDisconnected();
    Chat.removeWriting();
    Chat.msgInfo(__("Utracono połączenie z serwerem..."));
    Stats.reset();
  });

  var writingTimeout = null;
  var writing = false;

  $('#message-field').keyup(function (e) {
    if (e.keyCode === 13) {
      sendChatMsg();
    } else {
      if (writingTimeout !== null) {
        clearTimeout(writingTimeout);
        writingTimeout = null;
      }
      if (!writing) {
        writing = true;
        socket.emit("writing", writing);
      }
      writingTimeout = setTimeout(function () {
        if (writing) {
          writing = false;
          socket.emit("writing", writing);
        }
      }, 2000);
    }
  });

  $(window).on('beforeunload', function() {
    if (talkingNow) {
      return __('Czy na pewno chcesz przerwać trwającą rozmowę?');
    }
  });

  $(window).resize(function() {
    Chat.scrollToBottom();
  });

  $("#sound-volume-icon").mouseenter(function() {
    $("#sound-menu").stop().slideDown(200);
  });

  $("#sound-volume-box").mouseleave(function() {
    $("#sound-menu").stop().slideUp(200);
  });

  $("#sound-volume-box").click(function(e) {
    e.stopPropagation();
  });

  $("#sound-volume").change(function() {
    var volume = this.value / 100;
    AudioMgr.changeVolumeAndPlayTestSound(volume);
  });

  $('#sound-volume-max').click(function() {
    $("#sound-volume").val(100);
    AudioMgr.changeVolumeAndPlayTestSound(1);
  });

  $('#sound-volume-mute').click(function() {
    $("#sound-volume").val(0);
    AudioMgr.changeVolumeAndPlayTestSound(0);
  });

  modeDisconnected();
  Chat.msgInfo(__("Kliknij guzik \"Połącz\" na dole, by połączyć się z nieznajomym :)"));

  // Unmask contact link on click
  $('#contact-link').click(function() {
    this.href = this.href
      .replace(/_MALPA_/, '@')
      .replace(/_KROPKA_/, '.');
  });

  $('#emoticons-btn').click(function() {
    $('#emoticons-panel').toggle();
    Chat.scrollToBottom();
  });

  $('#emoticons-panel .emoticon').click(function() {
    if (talkingNow) {
      Chat.insertInput(this.title);
    }
  });

  // PARTNERS
  var $partners = $('#partners');
  var $partnersList = $partners.find('.partner');
  var totalPartners = $partnersList.length;

  function getRandomPartnerIndices(num) {
    var indices = [];

    for (var i = 0; i < totalPartners; ++i) {
      indices.push(i);
    }
    while (indices.length > num) {
      var index = Math.floor(Math.random() * indices.length);
      indices.splice(index, 1);
    }
    return indices;
  }

  function shufflePartners() {
    var $partnersList = $partners.find('.partner');
    for (var i = $partnersList.length; i >= 0; i--) {
      var j = Math.random() * i | 0;
      $partners.append($partnersList.get(j));
    }
  }

  function fadeInPartners(num) {
    var $partnersList = $partners.find('.partner');
    for (var i = 0; i < num; ++i) {
      $partnersList.eq(i)
        .delay(i * 100)
        .fadeIn();
    }
  }

  function fadeOutPartners(callback) {
    var $partnersList = $partners.find('.partner');
    var $visiblePartners = $partnersList.filter(':visible');
    $visiblePartners.each(function (index) {
      $(this)
        .delay(($visiblePartners.length - index - 1) * 100)
        .fadeOut(400, index == 0 ? callback : null);
    });
  }

  function getLimitOfVisiblePartners() {
    var visiblePartners = Math.floor(($('#aside').height() - 355) / 60);
    visiblePartners = Math.min(visiblePartners, 6);
    visiblePartners = Math.max(visiblePartners, 3);
    return visiblePartners;
  }

  var visiblePartners = getLimitOfVisiblePartners();
  shufflePartners();
  $partners.find('.partner').slice(visiblePartners).hide();

  setInterval(function() {
    if ($partners.is(':visible')) {
      var visibleLimit = getLimitOfVisiblePartners();
      var visiblePartners = $partnersList.filter(':visible').length;
      if (visibleLimit < totalPartners || visiblePartners < visibleLimit) {
        fadeOutPartners(function() {
          shufflePartners();
          fadeInPartners(visibleLimit);
        });
      }
    }
  }, 15000);
});
