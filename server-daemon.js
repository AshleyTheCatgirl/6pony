const fs = require('fs');
const child_process = require('child_process');

// Constants
const PID_FILE = './6pony.pid';

function stop() {
    if (fs.existsSync(PID_FILE)) {
        const pid = fs.readFileSync(PID_FILE, 'utf8');
        console.log('Killing PID ' + pid + '...');
        process.kill(pid);
        fs.unlinkSync(PID_FILE);
        return true;
    } else {
        return false;
    }
}

// Handle command line argument
if (process.argv.length >= 3) {
    const op = process.argv[2];
    if (op === 'stop') {
        if (stop()) {
            process.exit(0);
        } else {
            console.log('Process cannot be found (file ' + PID_FILE + ' does not exist).');
            process.exit(-1);
        }
    } else if (op === 'restart') {
        stop();
        // continue
    } else if (op == 'start') {
        if (fs.existsSync(PID_FILE)) {
            console.log('Process is already running (file ' + PID_FILE + ' exists).');
            process.exit(-1);
        }
    } else {
        console.log('Usage: ' + process.argv[1] + ' [start|stop|restart]');
        process.exit(-1);
    }
}

// Open log file
const log_file = fs.openSync('server.out', 'a');

// Start the child process
console.log('Starting daemon...');
const options = {
    detached: true,
    cwd: process.cwd(),
    stdio: ['ignore', log_file, log_file],
};
const child = child_process.spawn(process.execPath, ['server.js'], options);

// Save process PID
fs.writeFileSync(PID_FILE, child.pid);

// Remove child process reference so the parent can exit
child.unref();

// Exit parent
process.exit();
