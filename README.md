6pony
=====
Coś jak Omegle i 6obcy, tylko że kucykowe i dla Polaków.

Konfiguracja
------------
Przed uruchomieniem należy stworzyć plik `public/js/config.js` bazując na przykładzie znajdującym się w pliku `public/js/config.example.js` oraz plik `inc/config.js` bazując na `inc/config.example.js`.

Uruchomienie
------------
Backend uruchamia się poleceniem:

    nodejs server.js

Jest możliwość uruchomienia jako daemon (w tle):

    nodejs server-daemon.js

Daemon'a wyłącza się poleceniem:

    nodejs server-daemon.js stop

Optymalizacje dla produkcji włącza się ustawiając zmienną środowiskową NODE_ENV:
    export NODE_ENV=production
