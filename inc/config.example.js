exports.port = 1480;
exports.hostname = null;
exports.trackingUrl = null;
exports.trackingFileBaseName = 'piwik';
exports.trackingSiteId = 1;
exports.socketOrigins = 'http://0.0.0.0:1480 http://localhost:1480';
