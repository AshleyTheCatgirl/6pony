{
	"pl": "en",
	"pl_PL": "en_US",
	"6pony - poznawaj nowe kucyki!": "6pony - meet new ponies!",
	"Anonimowy czat dla bronies dzięki któremu poznasz wielu nowych znajomych.": "Anonymous chat for bronies by which you will make many new friends.",
	"Wyślij": "Send",
	"Połącz": "Connect",
	"Nieznajomy pisze wiadomość...": "Stranger is writing...",
	"Rozmawiających kucyków: %s.": "Talking ponies: %s.",
	"Oczekuje: %s.": "Waiting: %s.",
	"Kucyków online: %s (unikalnych %s).": "Ponies online: %s (%s unique).",
	"Nie odpowiadamy za to, co mówią spotkane tutaj osoby. Zalecamy nie udostępniać swoich prawdziwych danych osobowych.": "We are not responsible for the content of the messages you receive. We recommend that you do not share your personal information with strangers.",
	"<strong>UWAGA!</strong> Niniejsza strona wykorzystuje pliki cookies. Informacje uzyskane za pomocą cookies wykorzystywane są w celach statystycznych. Pozostając na stronie godzisz się na ich zapisywanie w Twojej przeglądarce. <a href=\"http://wszystkoociasteczkach.pl\" target=\"_blank\">Dowiedz się więcej.</a>": "<strong>Note:</strong> This site uses cookies. Information obtained through cookies are used for statistical purposes. By staying on this site, you agree to allow us to store them in your web browser. <a href=\"http://wszystkoociasteczkach.pl\" target=\"_blank\">Learn more.</a>",
	"Zamknij ostrzeżenie": "Close warning",
	"Emotikony": "Emotikony",
	"img/logo.png": "img/logo-en.png"
}