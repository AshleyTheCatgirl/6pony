{
	"pl": "pl",
	"pl_PL": "pl_PL",
	"6pony - poznawaj nowe kucyki!": "6pony - poznawaj nowe kucyki!",
	"Anonimowy czat dla bronies dzięki któremu poznasz wielu nowych znajomych.": "Anonimowy czat dla bronies dzięki któremu poznasz wielu nowych znajomych.",
	"Wyślij": "Wyślij",
	"Połącz": "Połącz",
	"Nieznajomy pisze wiadomość...": "Nieznajomy pisze wiadomość...",
	"Rozmawiających kucyków: %s.": "Rozmawiających kucyków: %s.",
	"Oczekuje: %s.": "Oczekuje: %s.",
	"Kucyków online: %s (unikalnych %s).": "Kucyków online: %s (unikalnych %s).",
	"Nie odpowiadamy za to, co mówią spotkane tutaj osoby. Zalecamy nie udostępniać swoich prawdziwych danych osobowych.": "Nie odpowiadamy za to, co mówią spotkane tutaj osoby. Zalecamy nie udostępniać swoich prawdziwych danych osobowych.",
	"<strong>UWAGA!</strong> Niniejsza strona wykorzystuje pliki cookies. Informacje uzyskane za pomocą cookies wykorzystywane są w celach statystycznych. Pozostając na stronie godzisz się na ich zapisywanie w Twojej przeglądarce. <a href=\"http://wszystkoociasteczkach.pl\" target=\"_blank\">Dowiedz się więcej.</a>": "<strong>UWAGA!</strong> Niniejsza strona wykorzystuje pliki cookies. Informacje uzyskane za pomocą cookies wykorzystywane są w celach statystycznych. Pozostając na stronie godzisz się na ich zapisywanie w Twojej przeglądarce. <a href=\"http://wszystkoociasteczkach.pl\" target=\"_blank\">Dowiedz się więcej.</a>",
	"Zamknij ostrzeżenie": "Zamknij ostrzeżenie",
	"Emotikony": "Emotikony",
	"img/logo.png": "img/logo.png"
}