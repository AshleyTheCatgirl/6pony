#!/bin/bash

SELF_PATH=`readlink -f "$0"`
SELF_DIR=`dirname "$SELF_PATH"`

echo "var emotIcons = {"

pushd "$SELF_DIR/../public/img/emots" >/dev/null
for name in *.png; do
    echo "  ':${name%.*}:': '$name',"
done
popd >/dev/null

echo "};";
