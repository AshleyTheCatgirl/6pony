const waitingQueue = []; // queue
const strangers = [];
const uniqueConnectedMap = {};
let talkingCount = 0;
let connectedCount = 0;
let uniqueConnectedCount = 0;
let lastVisitTime = null;

const config = require('./inc/config.js');

// Start HTTP server
const express = require('express');
const app = express();

const server = app.listen(config.port, config.hostname, () => {
  const host = server.address().address;
  const port = server.address().port;
  console.log('HTTP server is listening at http://%s:%s/', host, port);
});

// Start IO Server
const io = require('socket.io').listen(server);

// Cross-Origin configuration
if (config.socketOrigins) {
  io.origins(config.socketOrigins);
}

// IO Server events
io.sockets.on('connection', socket => { // when user connects...
  // update counter of connected users
  connectedCount++;
  socket.broadcast.emit('connected_count', connectedCount);
  console.log(`New user connected - connection count: ${connectedCount}`);

  const req = socket.request;
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  if (uniqueConnectedMap[ip] === undefined) {
    ++uniqueConnectedCount;
    uniqueConnectedMap[ip] = 1;
    socket.broadcast.emit('unique_count', uniqueConnectedCount);
  } else {
    ++uniqueConnectedMap[ip];
  }

  // send all stats to the new user
  socket.emit('connected_count', connectedCount);
  socket.emit('unique_count', uniqueConnectedCount);
  socket.emit('talking_count', talkingCount);
  socket.emit('waiting_count', waitingQueue.length);
  socket.emit('last_visit_time', lastVisitTime, uniqueConnectedCount);

  lastVisitTime = new Date().getTime();

  socket.on('get_stranger', () => { // ...and is looking for someone to chat...
    if (strangers[socket.id] !== undefined || waitingQueue.indexOf(socket) != -1) {
      return; // you are already waiting or talking wih someone
    }
    // let's try to find some stranger...
    const stranger = waitingQueue.shift();
    if (stranger === undefined) {
      // if there isn't any waiting user, put current user in the queue
      waitingQueue.push(socket);
      socket.broadcast.emit('waiting_count', waitingQueue.length);
      socket.emit('waiting_count', waitingQueue.length);
    } else {
      // there is someone who wants to talk
      strangers[socket.id] = stranger;
      strangers[stranger.id] = socket;
      socket.emit('found_stranger'); // ...tell them both...
      stranger.emit('found_stranger'); // ...that they're connected.
      talkingCount += 2;
      socket.broadcast.emit('talking_count', talkingCount);
      socket.emit('talking_count', talkingCount);
      socket.broadcast.emit('waiting_count', waitingQueue.length);
      socket.emit('waiting_count', waitingQueue.length);
      console.log(`Found a stranger - talking count: ${talkingCount}`);
    }
  });

  socket.on('disconnect', () => {
    // update counter of connected users
    connectedCount--;
    socket.broadcast.emit('connected_count', connectedCount);
    console.log(`User disconnected - connection count: ${connectedCount}`);

    --uniqueConnectedMap[ip];
    if (uniqueConnectedMap[ip] <= 0) {
      --uniqueConnectedCount;
      delete uniqueConnectedMap[ip];
      socket.broadcast.emit('unique_count', uniqueConnectedCount);
    }

    // update last visit time so new user can see it
    lastVisitTime = new Date().getTime();

    // check if he is actually waiting
    if (waitingQueue.indexOf(socket) !== -1)	{
      waitingQueue.splice(waitingQueue.indexOf(socket), 1);
      socket.broadcast.emit('waiting_count', waitingQueue.length);
    }

    // check if he is actually talking
    if (strangers[socket.id] !== undefined) {
      strangers[socket.id].emit('stranger_disconnected');
      strangers[strangers[socket.id].id] = undefined;
      strangers[socket.id] = undefined;
      talkingCount -= 2;
      socket.broadcast.emit('talking_count', talkingCount);
    }
  });

  socket.on('end_chat', () => {
    // check if he is actually waiting
    if (waitingQueue.indexOf(socket) !== -1)	{
      waitingQueue.splice(waitingQueue.indexOf(socket), 1);
      socket.broadcast.emit('waiting_count', waitingQueue.length);
      socket.emit('waiting_count', waitingQueue.length);
    }

    // check if he is actually talking
    if (strangers[socket.id] !== undefined) {
      strangers[socket.id].emit('stranger_disconnected');
      strangers[strangers[socket.id].id] = undefined;
      strangers[socket.id] = undefined;
      talkingCount -= 2;
      socket.broadcast.emit('talking_count', talkingCount);
      socket.emit('talking_count', talkingCount);
      console.log(`User ended a chat - talking count: ${talkingCount}`);
    }
  });

  socket.on('msg', text => {
    if (strangers[socket.id] !== undefined && (typeof text) == 'string') {
      text = text.substring(0, 512);
      strangers[socket.id].emit('msg', text);
    }
  });

  socket.on('writing', writing => {
    if (strangers[socket.id] !== undefined) {
      strangers[socket.id].emit('writing', writing);
    }
  });
});

// Handlebars templating engine
const exphbs = require('express-handlebars');
const hbs = exphbs.create({
  defaultLayout: 'main',
  extname: '.hbs',
  helpers: {
    concat(params) {
      return Array.prototype.slice.call(arguments, 0, -1).join('');
    },
  },
});

app.engine('hbs', hbs.engine);
app.set('view engine', 'hbs');

// Internationalization support
const i18n = require('i18n');
i18n.configure({
    directory: __dirname + '/locales',
    locales: ['en', 'pl'],
    defaultLocale: 'pl',
    cookie: 'locale',
    queryParameter: 'locale',
});
app.use(i18n.init);

// Static resources
app.use(express.static(__dirname + '/public'))

// API
app.get('/api/stats', (req, res) => {
  const stats = {
    talking: talkingCount,
    waiting: waitingQueue.length,
    connected: connectedCount,
    unique: uniqueConnectedCount,
  };
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(stats));
});

// Main page
const emotIcons = require('./inc/emoticons.json');
app.get('/', (req, res) => {
  res.render('home', {
    talkingCount: talkingCount,
    waitingCount: waitingQueue.length,
    connectedCount: connectedCount,
    uniqueCount: uniqueConnectedCount,
    emotIcons: emotIcons,
    trackingFileBaseName: config.trackingFileBaseName,
    trackingUrl: config.trackingUrl,
    trackingSiteId: config.trackingSiteId,
  });
});

// Admin Console
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
rl.setPrompt('Announce> ');
rl.prompt();

rl.on('line', line => {
  if (line) {
    console.log('Sending announcement:', line);
    io.emit('announcement', line);
  }
  rl.prompt();
});
